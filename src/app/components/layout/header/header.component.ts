import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor() {}
  open = false;

  ngOnInit(): void {}

  toggle() {
    this.open = !this.open;
    console.log(this.open, 'hello');
  }
}

import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';

@Component({
  selector: 'app-forgot-username',
  templateUrl: './forgot-username.component.html',
  styleUrls: ['./forgot-username.component.css'],
})
export class ForgotUsernameComponent implements OnInit {
  constructor(private location: Location) {}

  ngOnInit(): void {}
  backClicked() {
    this.location.back();
  }
}
